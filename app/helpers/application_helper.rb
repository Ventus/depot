module ApplicationHelper

  def hidden_ul_if(condition, attributes ={}, &block)
    if condition
      attributes["style"] = "display: none;"
    end
    content_tag("ul", attributes, &block)
  end

end
