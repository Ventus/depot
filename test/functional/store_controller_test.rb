require 'test_helper'

class StoreControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
    assert_select 'ul.nav li a', minimum: 4
    assert_select 'table tr', 3
    assert_select 'dt', 'Programming Ruby 1.9'
    assert_select 'code', /\$[,\d]|\.\d\d/
  end

  test "markup needed for store.js.coffee is in place" do
    get :index
    assert_select 'img', 3
    assert_select 'i.icon-shopping-cart', 3
  end

end
