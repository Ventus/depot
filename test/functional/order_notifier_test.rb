require 'test_helper'

class OrderNotifierTest < ActionMailer::TestCase
  test "received" do
    mail = OrderNotifier.received(orders(:one))
    assert_equal "Pragmatic Store Order Conirmation", mail.subject
    assert_equal ["dave@example.org"], mail.to
    assert_equal ["ventus@linqle.com"], mail.from
    assert_match %r{1 x Programming Ruby 1.9}, mail.body.encoded
  end

  test "shipped" do
    mail = OrderNotifier.shipped(orders(:one))
    assert_equal "Pragmatic Store Order Shipped", mail.subject
    assert_equal ["dave@example.org"], mail.to
    assert_equal ["ventus@linqle.com"], mail.from
    assert_match %r{<dt>1 &times; Programming Ruby 1.9<\/dt>}, mail.body.encoded
  end

end
